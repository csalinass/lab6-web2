<?php
namespace App\Models;
use CodeIgniter\Model;

class ContactUsModel extends Model {
    protected $table = 'contact_us';
    protected $primaryKey = 'ID'; // Corrección en el nombre de la propiedad
    protected $useAutoIncrement = true;
    protected $returnType = 'object';
    protected $allowedFields = [
        'name',
        'email',
        'message'
    ];
}
