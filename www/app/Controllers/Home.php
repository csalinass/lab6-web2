<?php

namespace App\Controllers;

use App\Models\ContactUsModel;

class Home extends BaseController
{
    protected $contactModel;

    public function __construct(){
        $this->contactModel = new ContactUsModel();
    }

    public function index(): string
    {
        // Cargar la vista perritos_view.php
        return view('perritos_view');
    }
    
    public function contact_keep(){
        $data = [
            'name' => $this->request->getVar('name'),
            'email' => $this->request->getVar('email'),
            'message' => $this->request->getVar('message')
        ];

        // Insertar datos en la base de datos usando el método insert del modelo
        $this->contactModel->insert($data);

        return $this->respond([
            'code' => 200
        ]);
    }
}
